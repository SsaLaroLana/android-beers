@file:Suppress("EXPERIMENTAL_API_USAGE")

package com.ssalarolana.beers

import androidx.test.core.app.ApplicationProvider
import com.ssalarolana.beers.network.BeersApiService
import com.ssalarolana.beers.repositories.BeersRepository
import com.ssalarolana.beers.ui.home.beers_list.BeersViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.*
import org.junit.runners.MethodSorters
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Local unit test, which will execute on the development machine (host).
 * With this test all Beers API are tested to check if request are well formed
 */
@HiltAndroidTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class BeersViewModelTest {

    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    private val mainThreadSurrogate = newSingleThreadContext("UI Thread")

    private lateinit var beersViewModel: BeersViewModel

    @Inject
    lateinit var beersRepository: BeersRepository

    @Before
    fun setUp() {

        Dispatchers.setMain(mainThreadSurrogate)

        hiltRule.inject()

        beersViewModel =
            BeersViewModel(ApplicationProvider.getApplicationContext(), beersRepository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun test_beers_repository() {
        assertThat(beersRepository, not(nullValue()))
    }

    @Test(timeout = 60000)
    fun test_beers_request_without_date_period() = runBlocking {

        beersRepository.getAllBeers(page = 1, beersPerPage = 50)
        val result = beersRepository.result.first()
        assert(result.isSuccess)
        val pair = result.getOrNull()

        val beers = pair?.second

        assertThat(beers, not(nullValue()))

        assertThat(beers?.size, `is`(50))
    }

    @Test(timeout = 60000)
    fun test_beers_request_empty_with_date_period() = runBlocking {
        beersRepository.getAllBeers(page = 1, beersPerPage = 50, startDate = Pair(1, 2021), endDate = Pair(6, 2021))
        val result = beersRepository.result.first()
        assert(result.isSuccess)
        val pair = result.getOrNull()

        val beers = pair?.second

        assertThat(beers, not(nullValue()))

        assertThat(beers?.size, `is`(0))

    }

    @Test(timeout = 60000)
    fun test_beers_request_not_emtpy_with_date_period() = runBlocking {
        beersRepository.getAllBeers(page = 1, beersPerPage = 50, startDate = Pair(1, 2010), endDate = Pair(12, 2010))
        val result = beersRepository.result.first()
        assert(result.isSuccess)
        val pair = result.getOrNull()

        val beers = pair?.second

        assertThat(beers, not(nullValue()))

        assert((beers?.size ?: 0) > 0)

    }


    // region Hilt dependency

    @Module
    @InstallIn(SingletonComponent::class)
    object AppModule {

        @Singleton
        @Provides
        fun provideBeersRepository(
            beersApiService: BeersApiService
        ): BeersRepository = BeersRepository(beersApiService)
    }

    // endregion

}