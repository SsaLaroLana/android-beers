package com.ssalarolana.beers

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */

@HiltAndroidApp
class BeersApplication: Application()