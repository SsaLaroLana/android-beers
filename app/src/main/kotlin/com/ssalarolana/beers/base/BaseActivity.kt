package com.ssalarolana.beers.base

import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.viewbinding.ViewBinding

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */
abstract class BaseActivity<VM : BaseViewModel, DB : ViewDataBinding, VB : ViewBinding> : AppCompatActivity() {

    // region Protected properties

    protected abstract val viewModel: VM
    protected lateinit var dataBinding: DB
    protected lateinit var viewBinding: VB

    // endregion

}