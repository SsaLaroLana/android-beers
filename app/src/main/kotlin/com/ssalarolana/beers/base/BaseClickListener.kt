package com.ssalarolana.beers.base

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 03/06/2021.
 */
interface BaseClickListener<M> {

    fun onClick(item: M?): Unit?
}