package com.ssalarolana.beers.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding


/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 03/06/2021.
 */
abstract class BaseFragment<VM : BaseViewModel, DB : ViewDataBinding, VB : ViewBinding> : Fragment() {

    // region Protected properties

    protected abstract val viewModel: VM
    protected lateinit var dataBinding: DB
    protected var viewBinding: VB? = null

    // endregion

    // region Lifecycle

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? = getRootView()

    override fun onDestroyView() {
        super.onDestroyView()
        viewBinding = null
    }

    // endregion

    // region Private

    private fun getRootView(): View? = when {
        this::dataBinding.isInitialized -> {
            dataBinding.lifecycleOwner = viewLifecycleOwner
            dataBinding.root
        }
        else -> viewBinding?.root
    }

    // endregion
}