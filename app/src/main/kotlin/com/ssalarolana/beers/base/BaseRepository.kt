package com.ssalarolana.beers.base

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */
abstract class BaseRepository<T>(protected val repositoryScope: CoroutineScope = CoroutineScope(Dispatchers.IO + Job())) {

    // RESULT HANDLER

    protected val _result: MutableSharedFlow<T> = MutableSharedFlow(replay = 1)
    internal val result: SharedFlow<T> = _result

    // region Protected

    // endregion

}