package com.ssalarolana.beers.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */

abstract class BaseViewModel(application: Application) : AndroidViewModel(application)
