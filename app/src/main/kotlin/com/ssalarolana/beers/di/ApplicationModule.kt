package com.ssalarolana.beers.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    fun applicationContext(application: Application): Context = application.applicationContext

}