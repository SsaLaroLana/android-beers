package com.ssalarolana.beers.extensions

import android.app.Activity
import android.content.Intent

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 04/06/2021.
 */
fun Activity.launch(cls: Class<*>, flags: Int? = null, finishCurrent: Boolean = false) {
    val intent = Intent(this, cls)
    flags?.let { intent.addFlags(flags) }
    startActivity(intent)
    if (finishCurrent) {
        this.finish()
    }
}