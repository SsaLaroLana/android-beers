package com.ssalarolana.beers.extensions

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 03/06/2021.
 */

fun RecyclerView.LayoutManager?.findLastVisibleItemPosition(): Int = (this as? LinearLayoutManager)?.findLastVisibleItemPosition() ?: -1

fun RecyclerView.LayoutManager?.findLastCompletelyVisibleItemPosition(): Int = (this as? LinearLayoutManager)?.findLastCompletelyVisibleItemPosition() ?: -1
