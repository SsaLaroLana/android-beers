package com.ssalarolana.beers.network

import com.ssalarolana.beers.di.NetworkModule.Companion.BEERS_URL
import com.ssalarolana.beers.storage.models.Beer
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */

interface BeersApiService {

    @GET(BEERS_URL)
    suspend fun getAllBeers(
        @Query("page") page: Int = 1,
        @Query("per_page") beersPerPage: Int = 50,
        @Query("brewed_after") startDate: Pair<Int, Int>? = null,
        @Query("brewed_before") endDate: Pair<Int, Int>? = null
    ): Response<List<Beer>?>

}