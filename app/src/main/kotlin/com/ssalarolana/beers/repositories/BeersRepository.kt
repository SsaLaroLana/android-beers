package com.ssalarolana.beers.repositories

import com.ssalarolana.beers.base.BaseRepository
import com.ssalarolana.beers.network.BeersApiService
import com.ssalarolana.beers.storage.models.Beer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 04/06/2021.
 */

/**
 * This Data Type represent the data model to handle Pagination.
 * Boolean value is to check if the new beers result is to append or not
 */
typealias Beers = Pair<Boolean, List<Beer>?>
typealias StartDate = Pair<Int, Int>
typealias EndDate = Pair<Int, Int>
typealias Period = Pair<StartDate?, EndDate?>

@Singleton
class BeersRepository @Inject constructor(
    private val beersApiService: BeersApiService
) : BaseRepository<Result<Beers>>() {

    // region Private properties

    private var currentStartDate: StartDate? = null
    private var currentEndDate: EndDate? = null
    private val _selectedPeriod: MutableSharedFlow<Period> = MutableSharedFlow(replay = 1)

    // endregion

    // region Public properties

    /**
     * Store selected period to handle view lifecycle (es. onConfigurationChanged)
     */
    internal val selectedPeriod: SharedFlow<Period> = _selectedPeriod

    // endregion

    // region Public

    /**
     *
     */
    internal suspend fun getAllBeers(page: Int, beersPerPage: Int) = getAllBeers(page, beersPerPage, currentStartDate, currentEndDate)

    /**
     * Retrieve All Beers of current Page with limit of "beersPerPage" for only single page.
     * If present require only beer between startDate and endDate
     *
     * @param page current Page of beers list
     * @param beersPerPage number of beers for single page
     * @param startDate to showing beers in base of brewed date
     * @param endDate to showing beers in base of brewed date
     */
    internal suspend fun getAllBeers(
        page: Int,
        beersPerPage: Int,
        startDate: StartDate? = null,
        endDate: EndDate? = null
    ) = withContext(Dispatchers.IO) {
        val result = try {

            val appendResult = (startDate == currentStartDate) and (endDate == currentEndDate)
            if (appendResult.not()) {
                currentStartDate = startDate
                currentEndDate = endDate
                _selectedPeriod.emit(Period(startDate, endDate))
            }

            val beers = beersApiService.getAllBeers(
                page = page,
                beersPerPage = beersPerPage,
                startDate = currentStartDate,
                endDate = currentEndDate
            ).body()
            Result.success(Beers(appendResult, beers))
        } catch (ex: Exception) {
            Result.failure(ex)
        }
        _result.emit(result)
    }

    // endregion
}