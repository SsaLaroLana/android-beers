package com.ssalarolana.beers.storage.models

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */
@JsonClass(generateAdapter = true)
@Parcelize
data class Beer (
    val id: Int,
    val name: String?,
    val tagline: String?,
    @Json(name = "first_brewed")
    val firstBrewed: String?,
    val description: String?,
    @Json(name = "image_url")
    val imageUrl: String?,
    val abv: Double?,
    val ibu: Double?,
    @Json(name = "target_fg")
    val targetFg: Int?,
    @Json(name = "target_og")
    val targetOg: Double?,
    val ebc: Double?,
    val srm: Double?,
    val ph: Double?,
    @Json(name = "attenuation_level")
    val attenuationLevel: Double?,
    @Json(name = "volume")
    val volume: Volume?,
    @Json(name = "boil_volume")
    val boilVolume: BoilVolume?,
    @Json(name = "method")
    val method: Method?,
    @Json(name = "ingredients")
    val ingredients: Ingredients?,
    @Json(name = "food_pairing")
    val foodPairing: List<String>?,
    @Json(name = "brewers_tips")
    val brewersTips: String?,
    @Json(name = "contributed_by")
    val contributedBy: String?
): Parcelable