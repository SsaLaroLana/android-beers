package com.ssalarolana.beers.storage.models

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 03/06/2021.
 */

@JsonClass(generateAdapter = true)
@Parcelize
data class BoilVolume(
    val value: Int?,
    val unit: String?
) : Parcelable