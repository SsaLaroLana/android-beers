package com.ssalarolana.beers.storage.models

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */

@JsonClass(generateAdapter = true)
@Parcelize
data class Ingredients (
    val malt: List<Malt>?,
    val hops: List<Hops>?,
    val yeast: String?
): Parcelable