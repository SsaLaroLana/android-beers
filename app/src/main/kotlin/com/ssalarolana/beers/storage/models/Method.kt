package com.ssalarolana.beers.storage.models

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 03/06/21.
 */
@JsonClass(generateAdapter = true)
@Parcelize
data class Method(
    @Json(name = "mash_temp")
    val mashTemp: List<MashTemp>?,
    val fermentation: Fermentation?
): Parcelable