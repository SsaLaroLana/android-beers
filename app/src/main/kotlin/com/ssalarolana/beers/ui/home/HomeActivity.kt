package com.ssalarolana.beers.ui.home

import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.google.android.material.snackbar.Snackbar
import com.ssalarolana.beers.R
import com.ssalarolana.beers.base.BaseActivity
import com.ssalarolana.beers.databinding.ActivityHomeBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.flowOn
import ru.beryukhov.reactivenetwork.ReactiveNetwork

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */
@AndroidEntryPoint
class HomeActivity: BaseActivity<HomeViewModel, Nothing, ActivityHomeBinding>() {

    // region Override properties

    override val viewModel: HomeViewModel by viewModels()

    // endregion

    // region Private properties

    private val navController by lazy {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.home_nav_host_fragment) as NavHostFragment
        navHostFragment.navController
    }
    private lateinit var appBarConfiguration: AppBarConfiguration

    // endregion

    // region Lifecycle

    init {
        /**
         * Observe Network status
         */
        lifecycleScope.launchWhenCreated {
            // Check Connection Status
            ReactiveNetwork()
                .observeInternetConnectivity()
                .drop(1)
                .catch { emit(false) }
                .flowOn(Dispatchers.IO)
                .collect { online ->
                    val message = if (online) R.string.beers_connection_status_online else R.string.beers_connection_status_offline
                    Snackbar.make(viewBinding.root, message, Snackbar.LENGTH_LONG).show()
                }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)


        // SETUP UI WITH NAVIGATION COMPONENT
        appBarConfiguration = AppBarConfiguration(navController.graph)
        NavigationUI.setupActionBarWithNavController(
            this@HomeActivity,
            navController,
            appBarConfiguration
        )

    }

    // endregion
}