package com.ssalarolana.beers.ui.home

import android.app.Application
import com.ssalarolana.beers.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */
@HiltViewModel
class HomeViewModel @Inject constructor(application: Application) : BaseViewModel(application) {
}