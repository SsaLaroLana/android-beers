package com.ssalarolana.beers.ui.home.beers_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ssalarolana.beers.R
import com.ssalarolana.beers.base.BaseClickListener
import com.ssalarolana.beers.databinding.ListItemBeerBinding
import com.ssalarolana.beers.repositories.Beers
import com.ssalarolana.beers.storage.models.Beer

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 04/06/2021.
 */
class BeersAdapter(
    private val clickListener: BaseClickListener<Beer>,
    private val beers: MutableList<Beer> = mutableListOf()
) : RecyclerView.Adapter<BeersAdapter.BeerViewHolder>() {


    // region Lifecycle

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder =
        BeerViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_beer, parent, false
            )
        )

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) =
        DataBindingUtil.bind<ListItemBeerBinding>(holder.itemView).run {
            this?.beer = beers[position]
            this?.clickListener = clickListener
        }

    override fun getItemCount(): Int = beers.size

    override fun getItemId(position: Int): Long = beers[position].id.toLong()

    // endregion

    // region Public

    internal fun addAll(beers: Beers) {
        if (beers.first) {
            beers.second?.let(this.beers::addAll)
            notifyItemInserted(this.beers.size.minus(beers.second?.size ?: 0))
        } else with(this.beers) {
            clear()
            addAll(beers.second ?: emptyList())
            notifyDataSetChanged()
        }
    }

    // endregion

    // region ViewHolder declaration

    inner class BeerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    // endregion
}