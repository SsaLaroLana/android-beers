package com.ssalarolana.beers.ui.home.beers_list

import android.icu.util.Calendar
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import com.ssalarolana.beers.R
import com.ssalarolana.beers.base.BaseClickListener
import com.ssalarolana.beers.base.BaseFragment
import com.ssalarolana.beers.databinding.FragmentBeersBinding
import com.ssalarolana.beers.extensions.findLastCompletelyVisibleItemPosition
import com.ssalarolana.beers.repositories.Beers
import com.ssalarolana.beers.storage.models.Beer
import com.ssalarolana.beers.utils.DividerSpaceItemDecoration
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.drop
import ru.beryukhov.reactivenetwork.ReactiveNetwork
import java.util.*


/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 03/05/2021.
 */
@AndroidEntryPoint
class BeersFragment : BaseFragment<BeersViewModel, FragmentBeersBinding, Nothing>(),
    BaseClickListener<Beer> {

    // region Override properties

    override val viewModel: BeersViewModel by viewModels()

    // endregion

    // region Private properties

    /**
     * Calculate Beers per page in base of Screen Height to perform a dynamically number of elements in base of device.
     */
    private val beersPerPage by lazy {
        val displayMetrics = DisplayMetrics()
        @Suppress("DEPRECATION")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) requireContext().display?.getRealMetrics(
            displayMetrics
        )
        else requireActivity().windowManager.defaultDisplay.getRealMetrics(displayMetrics)
        val greaterSize = if (displayMetrics.heightPixels > displayMetrics.widthPixels) displayMetrics.heightPixels else displayMetrics.widthPixels
        greaterSize / requireContext().resources.getDimension(R.dimen.list_item_beer_height).toInt()
    }

    /**
     * RecyclerView OnScrollLister to handle pagination when user reaches the end of the list
     * a new API call is made to request new beers
     */
    private val onScrollListener =
        object : RecyclerView.OnScrollListener() {

            var prevItemsCount: Int? = null

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val lastVisibleItem = recyclerView.layoutManager.findLastCompletelyVisibleItemPosition().inc()
                val itemsCount = recyclerView.layoutManager?.itemCount
                if (lastVisibleItem == itemsCount && itemsCount != prevItemsCount) {
                    dataBinding.loader.visibility = View.VISIBLE
                    viewModel.getAllBeers(itemsCount.div(beersPerPage).inc(), beersPerPage)
                }
            }
        }

    // endregion

    // region Lifecycle

    init {

        /**
         * Launch Coroutine when Fragment is in onCreate state and observe Beers result from ViewModel.
         * For this Job i have used a SharedFlow instead of LiveData.
         */
        lifecycleScope.launchWhenCreated {
            viewModel.result.collect { result -> handleBeersResult(result) }
        }
        /**
         * Notify UI when selected period of date (Start date and End date) changed
         */
        lifecycleScope.launchWhenCreated {
            viewModel.selectedPeriod.collect { period ->
                val isPeriodSelected = (period.first == null && period.second == null).not()
                val startDate = period.first // Selected StartDate with Month and Year
                val endDate = period.second // Selected EndDate with Month and Year

                with(dataBinding.selectedPeriod) {
                    text = getString(R.string.beers_selected_range, startDate?.first, startDate?.second, endDate?.first, endDate?.second)
                    visibility = if(isPeriodSelected) View.VISIBLE else View.GONE
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = FragmentBeersBinding.inflate(layoutInflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(dataBinding) {
            with(beers) {
                // Add Top Space between beer items
                addItemDecoration(DividerSpaceItemDecoration(resources.getDimension(R.dimen.margin_normal).toInt()))
                // Add adapter to show beers
                adapter = BeersAdapter(this@BeersFragment)
                // Add OnScrollListener to handle pagination
                addOnScrollListener(onScrollListener)
            }
            with(selectedPeriod) {
                setOnCloseIconClickListener {
                    selectedPeriod.visibility = View.GONE
                    viewModel.getAllBeers(beersPerPage = beersPerPage)
                }
                setOnClickListener { showCalendar() }
            }
            calendar.setOnClickListener { showCalendar() }
        }

        if (savedInstanceState == null)
            // Get First random beer to populate the screen when app starting
            viewModel.getAllBeers(beersPerPage = beersPerPage)
    }

    // endregion

    // region BaseClickListener implementation

    override fun onClick(item: Beer?): Unit? {
        // Doing thing after beer click, like drink a couple of beers...
        return null
    }

    // endregion

    // region Private

    /**
     * Show MaterialDatePicker in DateRange mode to select a Start Date and an End Date
     * to create a range in which to show the beers that fall within it
     *
     * BTW: The day of selected startDate and endDate will be ignored because API accept only Month and Year.
     */
    private fun showCalendar() {
        val picker = MaterialDatePicker
            .Builder
            .dateRangePicker()
            .setTitleText(R.string.beers_choose_dates_range)
            .build()


        picker.addOnPositiveButtonClickListener { range ->
            val calendar = Calendar.getInstance(Locale.getDefault())
            val start = range.first
            val end = range.second

            calendar.timeInMillis = start
            val startMonth = calendar[Calendar.MONTH].inc()
            val startYear = calendar[Calendar.YEAR]

            calendar.timeInMillis = end
            val endMonth = calendar[Calendar.MONTH].inc()
            val endYear = calendar[Calendar.YEAR]

            Log.d("START", "Month: $startMonth, Year: $startYear")
            Log.d("END", "Month: $endMonth, Year: $endYear")

            viewModel.getAllBeers(
                beersPerPage = beersPerPage,
                startDate = startMonth to startYear,
                endDate = endMonth to endYear
            )
        }
        picker.show(childFragmentManager, "data_range_picker")

    }

    /**
     * Handle Result of Beers request from API
     * @param result that contains new beers and append flag or an exception if request is unsuccessful
     */
    private fun handleBeersResult(result: Result<Beers>) {
        when (result.isSuccess) {
            true -> with(dataBinding) {

                // Create a little delay to refresh UI to simulate a loading...
                @Suppress("DEPRECATION")
                val handler =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) Handler.createAsync(Looper.getMainLooper())
                    else Handler()

                onScrollListener.prevItemsCount = beers.layoutManager?.itemCount

                handler.postDelayed({
                    (this.beers.adapter as? BeersAdapter)?.addAll(result.getOrThrow())
                    val empty = (this.beers.adapter?.itemCount ?: 0) <= 0
                    dataBinding.loader.visibility = View.GONE

                    if (empty and isEmpty) {
                        lottie.playAnimation()
                    } else {
                        isEmpty = empty
                    }
                }, 500)
            }
            else -> {
                dataBinding.loader.visibility = View.GONE

                Log.d(
                    "BEERS_REQUEST_ERROR",
                    result.exceptionOrNull()?.message ?: "Generic Error",
                    result.exceptionOrNull()
                )
            }
        }
    }

    // endregion
}