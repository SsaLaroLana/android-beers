package com.ssalarolana.beers.ui.home.beers_list

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.ssalarolana.beers.base.BaseViewModel
import com.ssalarolana.beers.repositories.Beers
import com.ssalarolana.beers.repositories.BeersRepository
import com.ssalarolana.beers.repositories.Period
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */
@HiltViewModel
class BeersViewModel @Inject constructor(
    application: Application,
    private val beersRepository: BeersRepository
) : BaseViewModel(application) {

    // region Public properties

    val result: SharedFlow<Result<Beers>> = beersRepository.result

    val selectedPeriod: SharedFlow<Period> = beersRepository.selectedPeriod

    // endregion

    // region Public

    internal fun getAllBeers(page: Int, beersPerPage: Int) = viewModelScope.launch {
        beersRepository.getAllBeers(page, beersPerPage)
    }

    internal fun getAllBeers(beersPerPage: Int, startDate: Pair<Int, Int>? = null, endDate: Pair<Int, Int>? = null) = viewModelScope.launch {
        beersRepository.getAllBeers(page = 1, beersPerPage, startDate, endDate)
    }

    // endregion
}