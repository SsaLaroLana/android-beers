package com.ssalarolana.beers.ui.splash

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.ssalarolana.beers.base.BaseActivity
import com.ssalarolana.beers.databinding.ActivitySplashScreenBinding
import com.ssalarolana.beers.extensions.launch
import com.ssalarolana.beers.ui.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 01/06/2021.
 */
@AndroidEntryPoint
class SplashScreenActivity: BaseActivity<SplashViewModel, Nothing, ActivitySplashScreenBinding>(), Animator.AnimatorListener {

    // region Override properties

    override val viewModel: SplashViewModel by viewModels()

    // endregion

    // region Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        with(viewBinding) {
            lottie.addAnimatorListener(this@SplashScreenActivity)
        }
    }

    // endregion

    // region Animator.AnimatorListener implementation

    override fun onAnimationEnd(animation: Animator) {
        launch(HomeActivity::class.java, Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK, finishCurrent = true)
    }

    override fun onAnimationCancel(animation: Animator) {}

    override fun onAnimationRepeat(animation: Animator) {}

    override fun onAnimationStart(animation: Animator) {}

    // endregion
}