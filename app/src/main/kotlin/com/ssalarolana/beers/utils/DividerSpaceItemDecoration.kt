package com.ssalarolana.beers.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 04/06/2021.
 */
class DividerSpaceItemDecoration(
    private val topSpacing: Int? = null
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        rect: Rect,
        view: View,
        parent: RecyclerView,
        s: RecyclerView.State,
    ) {
        topSpacing?.let {
            rect.top = topSpacing
        }

    }
}