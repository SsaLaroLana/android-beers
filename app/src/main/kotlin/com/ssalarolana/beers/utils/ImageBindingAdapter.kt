package com.ssalarolana.beers.utils

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import coil.load
import com.ssalarolana.beers.R


/**
 * Created by Marco Baldazzi, Senior Mobile Developer, on 04/06/2021.
 */
object ImageBindingAdapter {
    /**
     * LOADING IMAGE FROM URL WITH COIL
     */
    @BindingAdapter(value = ["android:src"])
    @JvmStatic
    fun setImageFromUrl(imageView: AppCompatImageView, url: String?) {
        url?.let {
            imageView.load(url) {
                crossfade(true)
                placeholder(R.drawable.ic_image_not_found)
            }
        } ?: imageView.load(R.drawable.ic_image_not_found) {
            crossfade(true)
        }
    }

}